// 百度翻译Api 中文翻译成英文 是一个辅助功能插件
import MD5 from "./md5.js";
const info = {
  apiServer:
    window.location.protocol === "http:"
      ? "http://api.fanyi.baidu.com/api/trans/vip/translate"
      : "https://fanyi-api.baidu.com/api/trans/vip/translate",
  from: "auto",
  to: "zh",
  appid: "20210413000777496",
};
window.jsonpCallback = function (response) {
  //   if (response && response.trans_result && response.trans_result.length) {
  //     const res = response.trans_result[0].dst
  //     if (res) {
  //       window.jsonpCallbackRes = res
  //     } else {
  //       window.jsonpCallbackRes = ''
  //     }
  //   }
  if (response) {
    const res = response;
    if (res) {
      window.jsonpCallbackRes = res;
    } else {
      window.jsonpCallbackRes = "";
    }
  }
};
function ajax(url, reslove) {
  // jsonp 请求 百度翻译接口
  const jsonp = document.createElement("script");
  jsonp.type = "text/javascript";
  jsonp.src = url + "&callback=jsonpCallback";

  document.getElementsByTagName("head")[0].appendChild(jsonp);
  jsonp.onload = (e) => {
    reslove(true);
  };
}

export default function translate(q = "") {
  const { from, to, appid, apiServer } = info;
  const salt = Date.parse(new Date()) / 1000;
  const params = {
    q,
    from,
    to,
    appid,
    salt,
    sign: MD5(appid + q + salt + "XQmuRngt9KYbGB7Fb27r"),
  };
  const url =
    apiServer +
    `?q=${q}&from=${from}&to=${to}&appid=${appid}&salt=${salt}&sign=${params.sign}`;
  return new Promise((reslove, reject) => {
    return ajax(url, reslove);
  }).then((res) => {
    return window.jsonpCallbackRes; // 返回翻译后的信息
  });
}
