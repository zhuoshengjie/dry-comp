import dryTranslater from "./src/trans_later";
// 为组件提供install安装方法，供按需引入
dryTranslater.install = (Vue) => {
  Vue.component(dryTranslater.name, dryTranslater);
};
// 默认导出组件
export default dryTranslater;
