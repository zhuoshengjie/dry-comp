// 整合所有的组件，对外导出，即一个完整的组件库
import dryTranslater from "./dryTransLater";
// 存储组件列表
const component = [dryTranslater];

// 定义install方法，接收Vue作为参数，如果使用use注册插件，则所有的组件都将被注册

const install = (Vue) => {
  // 判断是否安装
  if (install.installed) {
    return;
  }
  // 遍历所有组件
  component.map((item) => {
    Vue.component(item.name, item);
  });
};

// 判断是否引入文件
if (typeof window !== "undefined" && window.Vue) {
  install(window.Vue);
}

export default {
  // 导出的对象必须具有install,才能被Vue.use()方法安装
  install,
  dryTranslater,
};
