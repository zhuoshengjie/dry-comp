module.exports = {
    root: true,
    parser: "vue-eslint-parser",
    env: {
        browser: true,
        node: true,
        es6: true,
      },
    parserOptions: {
        "ecmaVersion": 7,
        "sourceType": "module"
      },
    rules: {
        "prettier/prettier": "off"
    }
}