# drycomp

vue选取文字实时翻译组件，欢迎使用  :stuck_out_tongue_winking_eye: :boom:  :star2: 

## 安装
```
npm i drycomp -S
```
## 使用
在mian.js中引入插件并注册
```
import drycomp from 'drycomp'
Vue.use(drycomp)
```
在页面中使用
```
<template>
    <drycomp />
</template>
```

## 作者主页
欢迎访问 [前端哔哔机](https://zhuoshengjie.github.io/).
