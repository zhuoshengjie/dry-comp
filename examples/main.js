import Vue from "vue";
import App from "./App.vue";
import dryTranslater from "../packages/index";

Vue.use(dryTranslater);

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount("#app");
